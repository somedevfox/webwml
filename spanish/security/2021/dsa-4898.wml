#use wml::debian::translation-check translation="282fe47153ee7ae459dbd068bec0e572c214acb8"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en wpa_supplicant y
en hostapd.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12695">CVE-2020-12695</a>

    <p>Se descubrió que hostapd no gestiona correctamente los mensajes
    de suscripción de UPnP en determinadas condiciones, permitiendo que un atacante
    provoque denegación de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-0326">CVE-2021-0326</a>

    <p>Se descubrió que wpa_supplicant no procesa correctamente la información
    de grupo P2P (Wi-Fi Direct) de los propietarios de grupos activos. Un
    atacante dentro del radio de alcance del dispositivo que ejecuta P2P podría
    aprovechar este defecto para provocar denegación de servicio o, potencialmente,
    para ejecutar código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27803">CVE-2021-27803</a>

    <p>Se descubrió que wpa_supplicant no procesa correctamente
    las peticiones de descubrimiento («provision discovery requests») P2P (Wi-Fi Direct). Un atacante
    dentro del radio de alcance del dispositivo que ejecuta P2P podría aprovechar
    este defecto para provocar denegación de servicio o, potencialmente, para ejecutar
    código arbitrario.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2:2.7+git20190128+0c1e29f-6+deb10u3.</p>

<p>Le recomendamos que actualice los paquetes de wpa.</p>

<p>Para información detallada sobre el estado de seguridad de wpa, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/wpa">https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4898.data"
