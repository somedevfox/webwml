#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a"
<define-tag pagetitle>Debian 11 actualizado: publicada la versión 11.1</define-tag>
<define-tag release_date>2021-10-09</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la primera actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction apr "Evita desreferencia de array fuera de límites">
<correction atftp "Corrige desbordamiento de memoria [CVE-2021-41054]">
<correction automysqlbackup "Corrige caída cuando se utiliza <q>LATEST=yes</q>">
<correction base-files "Actualizado para la versión 11.1">
<correction clamav "Nueva versión «estable» del proyecto original; corrige violaciones de acceso en clamdscan cuando --fdpass y --multipass se utilizan conjuntamente con ExcludePath">
<correction cloud-init "Evita includedir duplicado en /etc/sudoers">
<correction cyrus-imapd "Corrige problema de denegación de servicio [CVE-2021-33582]">
<correction dazzdb "Corrige un «uso tras liberar» en DBstats">
<correction debian-edu-config "debian-edu-ltsp-install: amplía la lista de exclusiones relativas al servidor principal; añade slapd y xrdp-sesman a la lista de servicios enmascarados">
<correction debian-installer "Recompilado contra proposed-updates; actualiza la ABI de Linux a la 5.10.0-9; usa udebs de proposed-updates">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates; usa udebs de proposed-updates y de «estable»; usa ficheros Packages comprimidos con xz">
<correction detox "Corrige tratamiento de ficheros grandes">
<correction devscripts "Hace que la opción --bpo apunte a bullseye-backports">
<correction dlt-viewer "Añade al paquete dev ficheros cabecera qdlt/qdlt*.h que faltaban">
<correction dpdk "Nueva versión «estable» del proyecto original">
<correction fetchmail "Corrige violación de acceso y regresión de seguridad">
<correction flatpak "Nueva versión «estable» del proyecto original; no hereda un valor inusual de $XDG_RUNTIME_DIR en el entorno aislado («sandbox»)">
<correction freeradius "Corrige caída de hilo y configuración de ejemplo">
<correction galera-3 "Nueva versión «estable» del proyecto original">
<correction galera-4 "Nueva versión «estable» del proyecto original; deja de proporcionar un paquete virtual <q>galera</q>, con lo que resuelve «Conflicts» circular con galera-3">
<correction glewlwyd "Corrige posible desbordamiento de memoria durante validación de firma FIDO2 al registrar webauthn [CVE-2021-40818]">
<correction glibc "Reinicia openssh-server incluso si ha sido desconfigurado durante la actualización; corrige el paso a modo texto cuando no se puede utilizar debconf">
<correction gnome-maps "Nueva versión «estable» del proyecto original; corrige caída al arrancar cuando el tipo del último mapa usado es aerial y no encuentra la definición de ningún teselado aerial; deja de escribir en ocasiones «broken last view position» («posición de la última vista inválida») al salir; corrige cuelgue al arrastrar marcas de rutas">
<correction gnome-shell "Nueva versión «estable» del proyecto original; corrige cuelgue tras cancelar (algunos) diálogos «system-modal»; corrige sugerencias de palabras en el teclado de la pantalla; corrige caídas">
<correction hdf5 "Ajusta dependencias para mejorar las actualizaciones desde versiones anteriores">
<correction iotop-c "Trata correctamente nombres de proceso en UTF-8">
<correction jailkit "Corrige la creación de jaulas que necesitan usar /dev; corrige comprobación de presencia de bibliotecas">
<correction java-atk-wrapper "Utiliza dbus también para detectar si la accesibilidad está habilitada">
<correction krb5 "Corrige caída del KDC por desreferencia nula en peticiones FAST que no incluyen el campo server [CVE-2021-37750]; corrige fuga de memoria en krb5_gss_inquire_cred">
<correction libavif "Usa el libdir correcto en el fichero pkgconfig de libavif.pc">
<correction libbluray "Cambia a libasm embebida; la versión de libasm-java es demasiado reciente">
<correction libdatetime-timezone-perl "Nueva versión «estable» del proyecto original; actualiza reglas de DST para Samoa y Jordania; confirmación de ausencia de segundo intercalar el 31 de diciembre de 2021">
<correction libslirp "Corrige varios problemas de desbordamiento de memoria [CVE-2021-3592 CVE-2021-3593 CVE-2021-3594 CVE-2021-3595]">
<correction linux "Nueva versión «estable» del proyecto original; incrementa la ABI a la 9; [rt] actualizado a la 5.10.65-rt53; [mipsel] bpf, mips: valida los desplazamientos de saltos condicionales [CVE-2021-38300]">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 9; [rt] actualizado a la 5.10.65-rt53; [mipsel] bpf, mips: valida los desplazamientos de saltos condicionales [CVE-2021-38300]">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 9; [rt] actualizado a la 5.10.65-rt53; [mipsel] bpf, mips: valida los desplazamientos de saltos condicionales [CVE-2021-38300]">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 9; [rt] actualizado a la 5.10.65-rt53; [mipsel] bpf, mips: valida los desplazamientos de saltos condicionales [CVE-2021-38300]">
<correction mariadb-10.5 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2021-2372 CVE-2021-2389]">
<correction mbrola "Corrige detección de fin de fichero">
<correction modsecurity-crs "Corrige problema de elusión de los cuerpos de solicitudes [CVE-2021-35368]">
<correction mtr "Corrige regresión en salida JSON">
<correction mutter "Nueva versión «estable» del proyecto original; kms: mejora el tratamiento de modos de vídeo comunes que podrían exceder el ancho de banda; asegura tamaño de textura de ventana válido tras cambiar la vista">
<correction nautilus "Al solicitar la apertura de varios ficheros evita la apertura de cada fichero en una instancia distinta de la aplicación; no guarda tamaño y posición de ventana al organizar como mosaico; corrige algunas fugas de memoria; actualiza traducciones">
<correction node-ansi-regex "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2021-3807]">
<correction node-axios "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2021-3749]">
<correction node-object-path "Corrige problemas de contaminación de prototipo [CVE-2021-23434 CVE-2021-3805]">
<correction node-prismjs "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2021-3801]">
<correction node-set-value "Corrige contaminación de prototipo [CVE-2021-23440]">
<correction node-tar "Borra rutas que no corresponden a directorios de la caché de directorios [CVE-2021-32803]; elimina rutas absolutas de forma más completa [CVE-2021-32804]">
<correction osmcoastline "Corrige las proyecciones distintas de la WGS84">
<correction osmpbf "Recompilado contra protobuf 3.12.4">
<correction pam "Corrige error de sintaxis en libpam0g.postinst cuando falla una unidad systemd">
<correction perl "Actualización de seguridad; corrige una fuga de memoria por expresión regular">
<correction pglogical "Incorpora correcciones para tratar snapshots de PostgreSQL 13.4">
<correction pmdk "Corrige falta de barreras tras memcpy no temporal">
<correction postgresql-13 "Nueva versión «estable» del proyecto original; corrige error de planificación de la aplicación repetida de un paso de proyección [CVE-2021-3677]; rechaza renegociación de SSL de forma más completa">
<correction proftpd-dfsg "Corrige <q>mod_radius leaks memory contents to radius server</q> («mod_radius filtra contenido de la memoria a servidor radius») y <q>sftp connection aborts with 'Corrupted MAC on input'</q> («conexión sftp aborta con 'MAC corrupta en la entrada'»); se salta la codificación para evitar la evaluación («escape») de texto SQL que ya ha pasado por este proceso">
<correction pyx3 "Corrige problema de alineación de tipos de letra con texlive 2020">
<correction reportbug "Actualiza los nombres de las distribuciones tras la publicación de bullseye">
<correction request-tracker4 "Corrige problema de ataque de canal lateral de sincronización en el acceso al sistema («login») [CVE-2021-38562]">
<correction rhonabwy "Corrige cálculo de etiqueta JWE CBC y verificación de firma JWS alg:none">
<correction rpki-trust-anchors "Añade URL HTTPS al TAL de LACNIC">
<correction rsync "Añade de nuevo --copy-devices; corrige regresión en --delay-updates; corrige caso límite en --mkpath; corrige rsync-ssl; corrige --sparce e --inplace; actualiza opciones disponibles para rrsync; correcciones de documentación">
<correction ruby-rqrcode-rails3 "Corrige compatibilidad con ruby-rqrcode 1.0">
<correction sabnzbdplus "Evita escape del directorio en función renamer [CVE-2021-29488]">
<correction shellcheck "Corrige la visualización de las opciones largas en la página de manual">
<correction shiro "Corrige problemas de elusión de autenticación [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510]; actualiza parche de compatibilidad con Spring Framework; soporte para Guice 4">
<correction speech-dispatcher "Corrige configuración del nombre de voz para el módulo generic">
<correction telegram-desktop "Evita caída con auto-delete habilitado">
<correction termshark "Incluye temas en el paquete">
<correction tmux "Corrige una condición de carrera que da lugar a que no se cargue la configuración si varios clientes están interactuando con el servidor mientras se inicializa">
<correction txt2man "Corrige regresión en el tratamiento de bloques">
<correction tzdata "Actualiza reglas de DST para Samoa y Jordania; confirmación de ausencia de segundo intercalar el 31 de diciembre de 2021">
<correction ublock-origin "Nueva versión «estable» del proyecto original; corrige problema de denegación de servicio [CVE-2021-36773]">
<correction ulfius "Se asegura de que la memoria está inicializada antes de usarla [CVE-2021-40540]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2021 4959 thunderbird>
<dsa 2021 4960 haproxy>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4965 libssh>
<dsa 2021 4966 gpac>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4968 haproxy>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4972 ghostscript>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4976 wpewebkit>
<dsa 2021 4977 xen>
<dsa 2021 4978 linux-signed-amd64>
<dsa 2021 4978 linux-signed-arm64>
<dsa 2021 4978 linux-signed-i386>
<dsa 2021 4978 linux>
<dsa 2021 4979 mediawiki>
</table>

<p>
Durante las etapas finales de la congelación de bullseye algunas actualizaciones se publicaron a través del
<a href="https://security.debian.org/">archivo de seguridad</a> pero sin el
correspondiente aviso de seguridad (DSA, por sus siglas en inglés). Detallamos a continuación estas actualizaciones.
</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction apache2 "Corrige inyección de línea en mod_proxy en peticiones HTTP2 [CVE-2021-33193]">
<correction btrbk "Corrige problema de ejecución de código arbitrario [CVE-2021-38173]">
<correction c-ares "Corrige ausencia de validación de la entrada en nombres de máquina devueltos por servidores DNS [CVE-2021-3672]">
<correction exiv2 "Corrige problemas de desbordamiento [CVE-2021-29457 CVE-2021-31292]">
<correction firefox-esr "Nueva versión «estable» del proyecto original [CVE-2021-29980 CVE-2021-29984 CVE-2021-29985 CVE-2021-29986 CVE-2021-29988 CVE-2021-29989]">
<correction libencode-perl "Encode: mitiga contaminación de @INC al cargar ConfigLocal [CVE-2021-36770]">
<correction libspf2 "spf_compile.c: corrige tamaño de ds_avail [CVE-2021-20314]; corrige modificador de macro <q>reverse</q>">
<correction lynx "Corrige fuga de credenciales si se usa SNI junto a un URL que contenga credenciales [CVE-2021-38165]">
<correction nodejs "Nueva versión «estable» del proyecto original; corrige problema de «uso tras liberar» [CVE-2021-22930]">
<correction tomcat9 "Corrige problema de elusión de autenticación [CVE-2021-30640] y problema de «contrabando» de peticiones («request smuggling») [CVE-2021-33037]">
<correction xmlgraphics-commons "Corrige problema de falsificación de solicitudes en el lado servidor («server side request forgery») [CVE-2020-11988]">
</table>


<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
