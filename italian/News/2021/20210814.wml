<define-tag pagetitle>Rilasciata Debian 11 <q>bullseye</q></define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff" maintainer="Giuseppe Sacco"

<p>Dopo due anni, un mese e nove giorni di sviluppo, il progetto Debian è
orgoglioso di presentare la sua versione stabile 11 (chiamata <q>bullseye</q>),
che verrà supportata per i prossimi 5 anni grazie al coordinamento del
<a href="https://security-team.debian.org/">gruppo per la sicurezza di Debian</a>
e del gruppo <a href="https://wiki.debian.org/LTS">Debian Long Term Support (Supporto Debian a lungo termine)</a>.
</p>

<p>
Debian 11 <q>bullseye</q> viene distribuita con diverse applicazioni ed ambienti desktop.
Tra gli altri ambienti grafici include:
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>

<p>Questo rilascio contiene oltre 11294 nuovi pacchetti per un totale di 59551 pacchetti,
oltre ad una significativa riduzione di oltre 9519 pacchetti che erano stati identificati
come <q>obsoleti</q> e rimossi. 42821 pacchetti sono stati aggiornati e 5434 sono
rimasti invariati.
</p>


<p>
<q>bullseye</q> è il nostro primo rilascio a fornire un kernel Linux che supporta
il file system exFAT e che lo utilizza automaticamente per montare file system exFAT.
Di conseguenza non è più necessario utilizzare implementazioni di file system
nello spazio utente fornite tramite il pacchetto exfat-fuse. 
Gli strumenti per creare e controllare file system exFAT sono inclusi nel pacchetto
exfatprogs.
</p>


<p>
Molte stampanti moderne sono utilizzabili, sia per la stampa che per la
scansione, senza driver specifici del venditore (spesso non liberi).

<q>bullseye</q> va oltre con un nuovo pacchetto, ipp-usb, che utilizza il protocollo
generico IPP-over-USB compatibile con diverse stampanti recenti. Questo permette di
considerare una stampante USB come un generico dispositivo di rete. Analogamente il
«backend» ufficiale di SANE è incluso nel pacchetto libsane1, si chiama sane-escl e
utilizza il protocollo eSCL.
</p>

<p>
Systemd in <q>bullseye</q> attiva, come impostazione predefinita, la funzionalità del
registro persistente con un ritorno automatico alla modalità volatile in caso di problemi.
Questo permette agli utenti che non hanno necessità particolari, di disinstallare l'attuale
demone del «logging» per utilizzare solo il registro di systemd.
</p>

<p>
Il gruppo Debian Med ha partecipato al contrasto del COVID-19 pacchettizzando
software per le ricerche virologiche a livello delle sequenze e per combattere
la pandemia con gli strumenti dell'epidemiologia; questo lavoro continuerà,
incentrato sugli strumenti per l'apprendimento automatico in entrambi i settori.
Il lavoro del gruppo integrato con quello del controllo qualità e dell'integrazione
continua è essenziale per ottenere i risultati riproducibili e coerenti 
richesti dalle scienze.

Debian Med Blend include una serie di applicazioni con alte richieste di prestazioni
che adesso possono beneficiare del «SIMD Everywhere». Per installare i pacchetti
gestiti dal gruppo Debian Med, selezionare i metapacchetti chiamati med-*,
giunti alla versione 3.6.x.
</p>

<p>
Cinese, giapponese, coreano e molte altre lingue hanno ora un nuovo metodo di
inserimento Fcitx 5, che è il successore del popolare Fcitx4 presente in <q>buster</q>;
questa nuova versione ha un migliore supporto per Wayland (il display manager predefinito).
</p>

<p>
Debian 11 <q>bullseye</q> include numerosi pacchetti software aggiornati (oltre il
72% di tutti i pacchetti della versione precedente), quali:
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux kernel 5.10 series</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>oltre 59000 altri pacchetti software pronti all'uso, realizzati a partire da
oltre 30000 pacchetti sorgente.</li>
</ul>

<p>
Attraverso questa ampia scelta di pacchetti ed il tradizionale vasto
supporto per diverse architetture, Debian ha concretizzato nuovamente lo scopo di essere
<q>il sistema operativo universale</q>. È ideale per differenti casi d'uso:
dai sistemi desktop ai netbook; dai server di sviluppo ai sistemi
«cluster»; nonché per server di database, web e «storage». Allo stesso tempo, 
per mezzo dell'impegno profuso in attività di controllo della qualità quali
ad esempio l'installazione automatica e i test di aggiornamento per tutti
i pacchetti dell'archivio di Debian, è stato possibile soddisfare con
<q>bullseye</q> le alte aspettative che gli utenti hanno nei confronti di 
un rilascio stabile di Debian.
</p>

<p>
Nel complesso sono supportate 9 architetture
64-bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bit PC / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
per ARM, <code>armel</code>
e <code>armhf</code> rispettivamente per i più vecchi e più recenti hardware a 32-bit
più <code>arm64</code> per le architetture <q>AArch64</q> a 64-bit,
per MIPS, <code>mipsel</code> (little-endian) architettura per hardware a 32-bit
e <code>mips64el</code> architettura per hardware a 64-bit little-endian.
</p>

<h3>Voglia di provarla per davvero?</h3>
<p>
Se si vuole semplicemente provare Debian 11 <q>bullseye</q> senza installarla,
si può utilizzare una delle <a href="$(HOME)/CD/live/">immagini live</a> disponibili
che avviano un sistema operativo completo in modalità di sola lettura 
utilizando la memoria del computer.
</p>

<p>
Queste immagini live sono presenti per le architetture <code>amd64</code> e 
<code>i386</code> e sono disponibili per DVD, memorie USB e per altre configurazioni
di avvio da rete. Le si possono scegliere tra vari ambienti desktop da provare:
GNOME, KDE Plasma, LXDE, LXQt, MATE e Xfce.
Debian Live <q>bullseye</q> utilizza una immagine standard live, in modo che
sia anche possibile provare un sistema Debian senza nessuna delle interfacce grafiche.
</p>

<p>
Se il sistema operativo è di proprio gradimento è anche possibile installarlo
direttamente dall'immagine live sul disco del computer. L'immagine
live include l'installatore indipendente Calamares oltre a quello standard Debian.
Maggiori informazioni sono disponibili nelle
<a href="$(HOME)/releases/bullseye/releasenotes">note di rilascio</a> e nella sezione
<a href="$(HOME)/CD/live/">immagini live installabili</a> del sito web Debian.
</p>

<p>
Per installare Debian 11 <q>bullseye</q> direttamente sul disco del computer
si può scegliere tra vari supporti di installazione quali dischi Blu-ray, DVD, CD,
memorie USB oppure tramite una connessione di rete.
Tramite queste immagini si possono installare vari ambienti desktop &mdash; Cinnamon,
GNOME, KDE Plasma Desktop and Applications, LXDE, LXQt, MATE e Xfce.
Inoltre ci sono i CD <q>multi-architettura</q> che permettono l'installazione su varie
architetture a partire da un unico CD. Naturalmente si può sempre preparare un supporto
USB avviabile (vedere la <a href="$(HOME)/releases/bullseye/installmanual">guida d'installazione</a>
per maggiori dettagli).
</p>

# Translators: some text taken from /devel/debian-installer/News/2021/20210802

<p>
Sono state fatte parecchie modifiche all'installatore Debian che
hanno portato ad un migliore supporto hardware e a nuove funzionalità.
</p>
<p>
In alcuni casi, una installazione completata correttamente può avere
ancora problemi alla gestione dello schermo dopo il riavvio; in questi
casi ci sono dei <a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">suggerimenti</a>
per accedere egualmente al sistema.
C'è anche la
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">procedura isenkram-based</a>
che permette di trovare e risolvere problemi per firmware mancante in maniera
automatizzata. Naturalmente vanno valutati pro e contro dell'utilizzo
di questo strumento poiché è molto probabile che richieda l'installazione
di pacchetti della sezione non-free.</p>

<p>
  Inoltre, le
  <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">immagini non-free dell'installatore che includono pacchetti firmware</a>
  sono state migliorate in modo soddisfare anticipatamente la necessità di firmware
  nel sistema installato (ad esempio, firmware per schede grafiche AMD o Nvidia,
  oppure nuove generazioni dell'hardware audio Intel).
</p>

<p>
Agli utenti cloud, Debian offre un supporto diretto per molte delle
piattaforme cloud in voga. Immagini ufficiali Debian sono facilmente
selezionabili nei vari «marketplace». Debian inoltre pubblica <a
href="https://cloud.debian.org/images/openstack/current/">immagini
OpenStack già preparate</a> per le architetture <code>amd64</code> e
<code>arm64</code>, pronte da scaricare e utilizzare in ambienti cloud
locali.
</p>

<p>
Debian può ormai essere installato in 76 lingue, per la maggior parte delle
quali sono disponibili sia l'interfaccia a caratteri che quella grafica.
</p>

<p>
Le immagini per l'installazione possono essere scaricate subito tramite
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (il metodo raccomandato),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> o
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; vedere
<a href="$(HOME)/CD/">Debian su CD</a> per altre informazioni. <q>bullseye</q> sarà
presto disponibile su supporti fisici DVD, CD-ROM e dischi Blu-ray tramite
numerosi <a href="$(HOME)/CD/vendors">rivenditori</a>.
</p>


<h3>Aggiornare Debian</h3>
<p>
Per la maggior parte delle configurazioni, gli aggiornamenti a Debian 11
dalla versione precedente, Debian 10 (nome <q>buster</q>) sono gestiti
automaticamente dallo strumento di gestione dei pacchetti APT.
</p>

<p>
La sezione degli aggiornamenti di bullseye si chiama bullseye-security
e ogni utente dovrebbe modificare la lista delle sorgenti di APT al
momento dell'aggiornamento.
Se la propria configurazione di APT utilizza il «pinning» o <code>APT::Default-Release</code>
è molto probabile che vada cambiata. Vedere la sezione sui
<a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">cambiamenti
al formato dell'archivio di sicurezza</a>
delle note di rilascio per maggiori dettagli.
</p>

<p>
Se si fa un aggiornamento da remoto, attenzione alla sezione
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">nessuna
nuova connessione SSH durante l'aggiornamento</a>. 
</p>

<p>
Come sempre, i sistemi Debian possono essere aggiornati facilmente, sul posto,
senza nessun periodo di fermo, ma è fortemente consigliato di leggere
le <a href="$(HOME)/releases/bullseye/releasenotes">note di rilascio</a> oltre
alla <a href="$(HOME)/releases/bullseye/installmanual">guida all'installazione</a>
per possibili problemi e per istruzioni dettagliate su installazione e aggiornamento.
Le note di rilascio verranno migliorate ulteriormente e tradotte in altre lingue
nelle settimane successive al rilascio.
</p>

<h2>Su Debian</h2>

<p>
Debian è un sistema operativo libero, sviluppato da centinaia di
volontari da tutte le parti del mondo che collaborano tramite
Internet. I punti di forza del progetto Debian sono la sua base di
volontari, la sua aderenza al contratto sociale Debian e al software
libero, e il suo impegno nell'offrire il miglior sistema operativo
possibile. Questo nuovo rilascio è un altro importante passo in
questa direzione.

<h2>Contatti</h2>

<p>Per maggiori informazioni, visitare le pagine web del sito Debian
<a href="$(HOME)/">https://www.debian.org/</a> o inviare un email a
&lt;press@debian.org&gt;.
</p>
