#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="c9a7e0f78250fe2fea728e669907c9ee47374e1c" maintainer="Luca Monducci"

#############################################################################

<div class="important">
<p><strong>
Questo port è stato abbandonato. Non ci sono stati aggiornamenti da ottobre
2002. Le informazioni in questa pagina hanno solo un valore storico.
</strong></p>
</div>


<h1>Debian GNU/NetBSD</h1>

<p>Debian GNU/NetBSD (i386) è un port del Sistema Operativo Debian sul kernel
NetBSD e libc (da non confondere con gli altri port Debian BSD basati su
glibc). Attualmente questo progetto è stato abbandonato (all'incirca a
ottobre 2002), quando ancora era nella fase iniziale dello sviluppo, comunque
era già possibile installarlo partendo da zero.</p>

<p>È stato anche tentato di realizzare un port Debian GNU/NetBSD (alpha),
poteva essere avviato da una chroot da un sistema NetBSD (alpha) ma era
possibile avviarlo in modo autonomo ed per funzionare utilizzava la maggior
parte delle librerie native di NetBSD. Questo è un <a
href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200201/msg00203.html">messaggio
di aggiornamento riguardo lo stato</a> inviato sulla lista.</p>


<h2>Notizie storiche</h2>

<dl class="gloss">
<dt class="new">06/10/2002:</dt>
<dd>
Sono disponibili dei dischetti sperimentali per l'installazione
di un sistema Debian GNU/NetBSD.</dd>

<dt>06/03/2002:</dt>
<dd>
Matthew ha modificato <a href="https://packages.debian.org/ifupdown">ifupdown</a>
e adesso è in uno stato usabile.</dd>

<dt>25/02/2002:</dt>
<dd>
Matthew ha reso noto che il supporto per shadow e PAM funzionano su
NetBSD. <a href="https://packages.debian.org/fakeroot">fakeroot</a>
sembra funzionare su FreeBSD, ma ha ancora dei problemi con NetBSD.</dd>

<dt>07/02/2002:</dt>
<dd>
Nathan ha da poco
<a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00091.html">reso
noto</a> che è riuscito ad avviare Debian GNU/FreeBSD in modalit&agrave;
multiutente. Inoltre sta lavorando su pacchetti di sola installazione
(usando debootstrap modificato) che si caratterizzano per il tarball
notevolmente più piccolo.</dd>

<dt>06/02/2002:</dt>
<dd>
Secondo Joel il gcc-2.95.4 ha superato la maggior parte della sua
suite di test ed è stato impacchettato.</dd>

<dt>06/02/2002:</dt>
<dd>
X11 funziona su NetBSD! Ancora onori a Joel Baker.</dd>

<dt>04/02/2002:</dt> 
<dd>
Primo passo verso un archivio Debian/*BSD:<br />
<a href="mailto:lucifer@lightbearer.com">Joel Baker</a> ha
<a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00067.html">annunciato</a>
un archivio <kbd>dupload</kbd>-abile per i pacchetti Debian di
FreeBSD e NetBSD.</dd>

<dt>03/02/2002:</dt>
<dd>
Debian GNU/NetBSD adesso è
<a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00043.html">self-hosting</a>!
Notare che richiede ancora NetBSD funzionante per l'installazione.</dd>

<dt>30/01/2002:</dt>
<dd>
Il port Debian GNU/*BSD adesso ha una pagina web!</dd>
</dl>


<h2>Perché Debian GNU/NetBSD?</h2>

<ul>
<li>NetBSD gira su hardware non supportato da Linux, il port di Debian
su kernel NetBSD incrementa il numero di piattaforme su cui si può
usare il sistema operativo Debian.</li>

<li>Il progetto Debian GNU/Hurd dimostra che Debian non è legata a un
kernel specifico, purtroppo il kernel Hurd continua a essere abbastanza
immaturo, invece un sistema Debian GNU/NetBSD dovrebbe essere usabile
su un sistema di produzione.</li>

<li>Le lezioni apprese con il port Debian su NetBSD possono essere usate
nel port di Debian su altri kernel (come quelli di <a
href="https://www.freebsd.org/">FreeBSD</a> e <a
href="http://www.openbsd.org/">OpenBSD</a>).</li>

<li>Al contrario di progetti come <a href="http://www.finkproject.org/">Fink</a>
Debian GNU/NetBSD non ha lo scopo di fornire del software aggiuntivo
o un ambiente stile Unix a un SO esistente (i port *BSD sono gi&agrave;
comprensivi e indiscutibilmente forniscono un ambiente stile Unix).
Invece un utente o un amministratore abituato a usare un sistema
Debian tradizionale si dovrebbe trovare immediatamente a proprio
agio con un sistema Debian GNU/NetBSD e diventare competente in un
periodo di tempo relativamente breve.</li>

<li>Non a tutti piacciono i port *BSD o lo spazio utente *BSD (questo
è un giudizio personale e non vuole essere in nessun modo un commento
sulla qualità). Sono state prodotte delle distribuzioni Linux che
forniscono dei port o uno spazio utente in stile *BSD per soddisfare
coloro a cui piace l'ambiente *BSD ma che vogliono usare un kernel
Linux. Debian GNU/NetBSD è la soluzione logicamente opposta, permette
alle persone a cui piace lo spazio utente GNU o un sistema di pacchetti
in stile Linux di usare il kernel NetBSD.</li>

<li>Perché noi possiamo.</li>
</ul>


<h2>Risorse</h2>

<p>Esiste una lista di messaggi Debian GNU/*BSD. La maggior parte delle
discussioni sul port sono avvenute sulla lista, gli archivi sono disponibili
su <url "https://lists.debian.org/debian-bsd/" />.</p>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
