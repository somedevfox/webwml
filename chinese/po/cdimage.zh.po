#
# Yangfl <mmyangfl@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2017-08-07 18:05+0800\n"
"Last-Translator: Yangfl <mmyangfl@gmail.com>\n"
"Language-Team:  <debian-l10n-chinese@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "[CN:密钥指纹:][HKTW:      金鑰指紋:]"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr "ISO 映像"

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr "Jigdo 文件"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />常見問題"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "透過 Jigdo 下載"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "透過 HTTP/FTP 下載"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "購買 CD 或 DVD"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "網絡安裝"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />下載"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />其他"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />美工"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />鏡像"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Rsync 鏡像"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />验证"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />透過 Torrent 下載"

#: ../../english/template/debian/cdimage.wml:49
msgid "Debian CD team"
msgstr "<span lang=\"en\">Debian </span>光碟小組"

#: ../../english/template/debian/cdimage.wml:52
msgid "debian_on_cd"
msgstr "Debian 光盘"

#: ../../english/template/debian/cdimage.wml:55
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />faq"

#: ../../english/template/debian/cdimage.wml:58
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:61
msgid "http_ftp"
msgstr "http/ftp"

#: ../../english/template/debian/cdimage.wml:64
msgid "buy"
msgstr "购买"

#: ../../english/template/debian/cdimage.wml:67
msgid "net_install"
msgstr "网络安装"

#: ../../english/template/debian/cdimage.wml:70
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />其他"

#: ../../english/template/debian/cdimage.wml:73
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr "CD/DVD <a href=\"/MailingLists/disclaimer\">公開郵件列表</a>（英文）："

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />映像檔釋出訊息"
