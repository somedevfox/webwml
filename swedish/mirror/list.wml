#use wml::debian::template title="Debians världsomspännande spegelplatser" BARETITLE=true
#use wml::debian::translation-check translation="c1ef6c3811be792e129656bbfcf09866d9880eb5"

<p>Debian finns spritt (<em>speglat</em>) på hundratals
servrar över Internet. Att använda en näraliggande server kommer troligen att snabba upp
dina nedladdningar, och även reducera trycket på våra centrala servrar och
på Internet i sin helhet.</p>

<p class="centerblock">
   Debianspeglingar finns i många länder, och för några har vi lagt
   till ett <code>ftp.&lt;country&gt;.debian.org</code>-alias. Detta
   alias pekar vanligtvis till en spegling som synkroniserar regelbundet
   och snabbt och som innehåller alla Debians arkitekturer. Debianarkivet
   finns alltid tillgängligt via <code>HTTP</code> på platsen
   <code>/debian</code> på servern.
</p>

<p class="centerblock">
   Andra <strong>speglingssajter</strong> kan ha restriktioner på vad
   de speglar (på grund av utrymmesrestriktioner). Bara på grund av att
   en sajt inte är landets <code>ftp.&lt;country&gt;.debian.org</code> betyder
   inte nödvändigtvis att den är långsammare eller mindre uppdaterad än
   <code>ftp.&lt;country&gt;.debian.org</code>-speglingen. Faktum är att
   en spegling som innehåller din arkitektur och är närmare dig som användare,
   och därmed snabbare, föredras nästan alltid före andra speglingar som
   är längre bort.
</p>

<p>Använd platsen närmast dig för snabbast möjliga nedladdning
oavsett om det är landets speglingsalias eller inte.
Programmet
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> kan användas för att
avgöra vilken plats som har minst latens; använd ett hämtningsprogram såsom
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> eller
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> för att avgöra vilken plats som ger högst nedladdningshastigheter.
Notera att geografisk närhet ofta inte är den viktigaste faktorn för att avgöra
vilken maskin som kommer ge dig högst hastigheter och pålitlighet.</p>

<p>
Om ditt system flyttas mycket så kan du vara bäst betjänt av en "spegling"
som drivs av en global <abbr title="Content Delivery Network - Innehållsleveransnätverk">CDN</abbr>.
Debianprojektet underhåller <code>deb.debian.org</code> för detta
ändamål och du kan använda det i din sources.list för apt &mdash; Se
<a href="http://deb.debian.org">tjänstens webbplats för ytterligare detaljer</a>.

Om du inte vet vilken spegling du skall använda eller om ditt system rör sig
mycket, kan du använda dig av <a href="http://httpredir.debian.org/">\
speglingsomdirigeringstjänsten</a> i din sources.list.
Den omdirigerar dynamiskt pakethämtningsförfrågningar till den bästa
tillgängliga speglingen baserat på ett antal faktorer så som spegelns
tillgänglighet, placering, arkitekturer och fräschhet. Använd denna adress i din
<a href="https://manpages.debian.org/man/5/sources.list">sources.list</a>
för att använda speglingsomdirigeraren:
</p>
<code>
http://httpredir.debian.org/debian
</code>

<p>
Allt annat du vill veta om Debians speglar:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">Debians speglingsalias per land</h2>

<table border="0" class="center">
<tr>
  <th>Land</th>
  <th>Plats</th>
  <th>Arkitekturer</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Lista på speglingar av Debianarkivet</h2>

<table border="0" class="center">
<tr>
  <th>Värdnamn</th>
  <th>HTTP</th>
  <th>Arkitekturer</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
