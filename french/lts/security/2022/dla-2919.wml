#use wml::debian::translation-check translation="06475b517e27a222487a8ec57aecfe7c18c76313" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes ont été découverts dans python2.7 :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3177">CVE-2021-3177</a>

<p>Python a un dépassement de tampon dans PyCArg_repr dans
_ctypes/callproc.c, qui pourrait conduire à l'exécution de code à distance
dans certaines applications Python qui acceptent des nombres à virgule
flottante comme entrée non fiable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4189">CVE-2021-4189</a>

<p>Un défaut a été découvert dans Python, particulièrement dans la
bibliothèque client FTP (File Transfer Protocol) lors de son utilisation en
mode PASV (passive). Le défaut réside dans comment le client FTP fait
confiance à l'hôte par défaut à partir de la réponse PASV. Un attaquant
pourrait utiliser ce défaut pour configurer un serveur FTP malveillant peut
entraîner des clients FTP à se reconnecter à une adresse IP et à un port
arbitraires. Cela pourrait conduire à ce qu'un client FTP analyse des ports
ce qui autrement n'aurait pas été possible.

Plutôt que d'utiliser l'adresse renvoyée, ftplib utilise maintenant
l'adresse IP à laquelle nous sommes déjà connectés. Pour l'utilisateur
particulier qui veut retourner à l'ancien comportement, il faut qu'il règle
un attribut « trust_server_pasv_ipv4_address » sur son instance
« ftplib.FTP » à True.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.7.13-2+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python2.7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python2.7, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python2.7">\
https://security-tracker.debian.org/tracker/python2.7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2919.data"
# $Id: $
