#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Voici une mise à jour de la DLA-558-1. La construction précédente
portait un numéro de révision considéré comme inférieur à celui dans Wheezy
et par conséquent n'a été pas installée lors de la mise à niveau.</p>

<p>Le texte de la DLA-558-1 est inclus pour référence (avec quelques
améliorations).</p>

<p>Deux problèmes de sécurité ont été découverts dans le paquet mongodb,
liés à la journalisation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6494">CVE-2016-6494</a>

<p>fichier d'historique .dbshell lisible par tous</p></li>

<li>Bogue Debian n° 833087

<p>authentification par question-réponse vulnérable à des attaques par
force brute dans un fichier journal non protégé</p></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.0.6-1.1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mongodb.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-588-2.data"
# $Id: $
