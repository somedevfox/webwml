#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Quelques problèmes de sécurité mineurs ont été identifiés et corrigés
dans binutils dans Debian LTS. Ce sont :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2226">CVE-2016-2226</a>

<p>Un dépassement de tampon exploitable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4487">CVE-2016-4487</a>

<p>Une écriture non valable à cause de l'utilisation de mémoire après
libération dans un tableau btypevec.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4488">CVE-2016-4488</a>

<p>Une écriture non valable à cause de l'utilisation de mémoire après
libération dans un tableau btypevec.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4489">CVE-2016-4489</a>

<p>Une écriture non valable à cause d'un dépassement d'entier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4490">CVE-2016-4490</a>

<p>Une violation d'accès en écriture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4492">CVE-2016-4492</a>

<p>Des violations d'accès en écriture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4493">CVE-2016-4493</a>

<p>Des violations d'accès en lecture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6131">CVE-2016-6131</a>

<p>Un dépassement de pile lors de l'affichage de mauvais octets dans des
objets HEX Intel.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.22-8+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets binutils.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-552.data"
# $Id: $
