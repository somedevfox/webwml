#use wml::debian::translation-check translation="b0ba430e78f27bb2ba75a7c94ec6d613245fc092" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Diverses vulnérabilités ont été corrigées dans libexif, une bibliothèque pour
analyser les fichiers de métadonnées EXIF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6328">CVE-2016-6328</a>

<p>Un dépassement d'entier lors de l’analyse des données d’entrée MNOTE dans le
fichier d’entrée a été découvert. Cela pouvait causer un déni de service et une
divulgation d'informations (divulgation de métadonnées critiques de partie du
tas, ou même des données privées d’autres applications).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7544">CVE-2017-7544</a>

<p>libexif était vulnérable à une lecture hors limites de tas dans la fonction
exif_data_save_data_entry dans libexif/exif-data.c, causée par un calcul
incorrect de longueur des données allouées dans une entrée ExifMnote qui
pouvait provoquer un déni de service ou éventuellement une divulgation
d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20030">CVE-2018-20030</a>

<p>Une erreur lors du traitement des balises EXIF_IFD_INTEROPERABILITY et
EXIF_IFD_EXIF dans la version de libexif pouvait être exploitée pour épuiser les
ressources disponibles de CPU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0093">CVE-2020-0093</a>

<p>Dans exif_data_save_data_entry de exif-data.c, il existait une possibilité de
lecture hors limites due à une vérification manquante de limites. Cela pouvait
amener une divulgation locale d'informations sans besoin de droits d’exécution
supplémentaires. Une interaction de l’utilisateur était nécessaire pour son
exploitation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12767">CVE-2020-12767</a>

<p>libexif comportait une erreur de division par zéro dans exif_entry_get_value
dans exif-entry.c.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.6.21-2+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libexif.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2214.data"
# $Id: $
