#use wml::debian::translation-check translation="04ff198945ee1e2710d939a1297baa51a03101e6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans l’analyseur du protocole
réseau, Wireshark.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10894">CVE-2019-10894</a>

<p>Plantage du dissecteur GSS-API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10895">CVE-2019-10895</a>

<p>Plantage de l’analyseur de fichier NetScaler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10896">CVE-2019-10896</a>

<p>Plantage du dissecteur DOF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10899">CVE-2019-10899</a>

<p>Plantage du dissecteur SRVLOC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10901">CVE-2019-10901</a>

<p>Plantage du dissecteur LDSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10903">CVE-2019-10903</a>

<p>Plantage du dissecteur DCERPC SPOOLSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12295">CVE-2019-12295</a>

<p>Moteur de dissection pouvant planter.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.6.8-1.1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wireshark, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wireshark">https://security-tracker.debian.org/tracker/wireshark</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2423.data"
# $Id: $
