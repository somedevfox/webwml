#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Vorbis-tools est vulnérable à plusieurs problèmes pouvant aboutir à un déni
de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9638">CVE-2014-9638</a>

<p>Erreur de division par zéro dans oggenc avec un fichier WAV dont le nombre de
canaux est réglé à zéro.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9639">CVE-2014-9639</a>

<p>Dépassement d’entier dans oggenc à l'aide d'un nombre contrefait de canaux
dans un fichier WAV, déclenchant un accès en mémoire hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9640">CVE-2014-9640</a>

<p>Lecture hors limites dans oggenc à l'aide d'un fichier raw contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6749">CVE-2015-6749</a>

<p>Dépassement de tampon dans la fonction aiff_open dans oggenc/audio.c
à l'aide d'un fichier AIFF contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.4.0-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vorbis-tools.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1010.data"
# $Id: $
