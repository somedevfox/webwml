#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette DLA corrige un problème dans l’analyse de certificats x509, un
 dépassement d'entier de pickle et quelques problèmes mineurs :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0772">CVE-2016-0772</a>

<p>La bibliothèque smtplib dans CPython ne renvoie pas une erreur quand StartTLS
échoue. Cela pourrait permettre à des attaquants de type « homme du milieu » de
contourner les protections TLS en exploitant une position de réseau entre le
client et le registre pour bloquer la commande StartTLS, autrement dit, une
« StartTLS stripping attack ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5636">CVE-2016-5636</a>

<p>Un dépassement d’entier dans la fonction get_data de zipimport.c dans CPython
permet à des attaquants distants d’avoir un impact non précisé à l'aide d'une
taille de données négative, déclenchant un dépassement de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5699">CVE-2016-5699</a>

<p>Une vulnérabilité d’injection CRLF dans la fonction HTTPConnection.putheader
d’urllib2 et urllib dans CPython permet à des attaquants distants d’injecter des
en-têtes arbitraires HTTP à l’aide de séquences CRLF dans un URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20406">CVE-2018-20406</a>

<p>Modules/_pickle.c possède un dépassement d'entier à l'aide d'une grande valeur
LONG_BINPUT mal gérée lors d’un essai <q>resize to twice the size</q>. Ce
problème peut causer un épuisement de mémoire, mais n’existe que si le
format de pickle est utilisé pour sérialiser des dizaines ou des centaines de
gigaoctets de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5010">CVE-2019-5010</a>

<p>Un déréférencement de pointeur NULL en utilisant un certificat X509
 contrefait pour l'occasion X509.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 3.4.2-1+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python3.4.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1663.data"
# $Id: $
