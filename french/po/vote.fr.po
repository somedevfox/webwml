# Templates files for webwml modules
# Copyright (C) 2003 Software in the Public Interest, Inc.
# Pierre Machard <pmachard@debian.org>
#
# Baptiste Jammet <baptiste@mailoo.org>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: debian webwml templates 0.1\n"
"PO-Revision-Date: 2019-12-25 19:35+0100\n"
"Last-Translator: Baptiste Jammet <baptiste@mailoo.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Date"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Calendrier"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Résumé"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nominations"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Retraits"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Débat"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Plates-formes"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Déposant"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Déposant de la proposition A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Déposant de la proposition B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Déposant de la proposition C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Déposant de la proposition D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Déposant de la proposition E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Déposant de la proposition F"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "Déposant de la proposition G"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "Déposant de la proposition H"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Parrains"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Parrains de la proposition A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Parrains de la proposition B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Parrains de la proposition C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Parrains de la proposition D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Parrains de la proposition E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Parrains de la proposition F"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "Parrains de la proposition G"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "Parrains de la proposition H"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Opposition"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Texte"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Proposition A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Proposition B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Proposition C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Proposition D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Proposition E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Proposition F"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "Proposition G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "Proposition H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Choix"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Déposant de l'amendement"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Parrains de l'amendement"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Texte de l'amendement"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Déposant de l'amendement A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Parrains de l'amendement A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Texte de l'amendement A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Déposant de l'amendement B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Parrains de l'amendement B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Texte de l'amendement B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Déposant de l'amendement C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Parrains de l'amendement C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Texte de l'amendement C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Amendements"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Compte-rendu"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Majorités requises"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Données et statistiques"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Quorum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Discussion minimale"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Bulletin"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Forum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Résultat"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "En&nbsp;attente&nbsp;de&nbsp;sponsors"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "En&nbsp;discussion"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Vote&nbsp;ouvert"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Décidé"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Annulé"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Autre"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Page&nbsp;d'accueil&nbsp;des&nbsp;votes"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Aide"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Soumettre&nbsp;une&nbsp;proposition"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Amender&nbsp;une&nbsp;proposition"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Appuyer&nbsp;une&nbsp;proposition"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Lire&nbsp;un&nbsp;résultat"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Vote"
