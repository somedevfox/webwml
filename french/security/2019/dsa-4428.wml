#use wml::debian::translation-check translation="9969b1c993a42ac9607c8814c1e94d4e5a39712e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Jann Horn a découvert que le module PAM dans systemd n'utilise pas de
façon sûre l'environnement et manque de vérification de station de travail
(« seat ») permettant d'usurper une session active de PolicyKit. Un attaquant
distant avec un accès SSH peut tirer avantage de ce problème pour obtenir les
droits de PolicyKit qui sont normalement accordés seulement aux clients dans
une session active sur la console locale.</p>

<p>Pour la distribution stable (Stretch), ce problème a été corrigé dans la
version 232-25+deb9u11.</p>

<p>Cette mise à jour fournit aussi des mises à jour qui étaient à l'origine
prévues pour la prochaine mise à jour mineure de Stretch 9.9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de systemd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/systemd">\
https://security-tracker.debian.org/tracker/systemd</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4428.data"
# $Id: $
