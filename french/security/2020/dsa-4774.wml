#use wml::debian::translation-check translation="43f8d7b8b91b167696b5c84ec0911bab7b7073f2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à l'exécution de code arbitraire, à une élévation de
privilèges, à un déni de service ou à des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12351">CVE-2020-12351</a>

<p>Andy Nguyen a découvert un défaut dans l'implémentation du Bluetooth
dans la manière dont les paquets L2CAP avec A2MP CID étaient gérés. Un
attaquant distant à faible distance connaissant l'adresse du périphérique
Bluetooth de la victime peut envoyer un paquet l2cap malveillant et
provoquer un déni de service ou éventuellement l'exécution de code
arbitraire avec les privilèges du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12352">CVE-2020-12352</a>

<p>Andy Nguyen a découvert un défaut dans l'implémentation du Bluetooth. La
mémoire de pile n'est pas correctement initialisée lors du traitement de
certains paquets AMP. Un attaquant distant à faible distance connaissant
l'adresse du périphérique Bluetooth de la victime peut récupérer des
informations de la pile du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25211">CVE-2020-25211</a>

<p>Un défaut a été découvert dans le sous-système netfilter. Un attaquant
local capable d'injecter une configuration de conntrack netlink peut
provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25643">CVE-2020-25643</a>

<p>ChenNan du Chaitin Security Research Lab a découvert un défaut dans le
module hdlc_ppp. Une validation d'entrée incorrecte dans la fonction
ppp_cp_parse_cr() pourrait conduire à une corruption de mémoire et à la
divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25645">CVE-2020-25645</a>

<p>Un défaut a été découvert dans le pilote d'interface pour le trafic
encapsulé de GENEVE quand il est combiné avec IPsec. Si IPsec est
configuré pour chiffrer le trafic pour le port UDP particulier utilisé par
le tunnel GENEVE, les données tunnelisées ne sont pas routées correctement
sur le lien chiffré mais plutôt envoyées sans chiffrement.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.19.152-1. Les vulnérabilités sont corrigées en basant la
version sur la nouvelle version amont stable 4.19.152 qui comprend des
corrections de bogues supplémentaires.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4774.data"
# $Id: $
