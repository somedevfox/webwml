#use wml::debian::translation-check translation="2592e40c5d7143a6f575ff96f6127ba4fb3f18d5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le serveur web HTTPD
Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1927">CVE-2020-1927</a>

<p>Fabrice Perez a signalé que certaines configurations de mod_rewrite sont
prédisposées à une redirection ouverte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1934">CVE-2020-1934</a>

<p>Chamal De Silva a découvert que le module mod_proxy_ftp utilise de la
mémoire non initialisée lorsqu'il sert de mandataire pour un dorsal FTP
malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9490">CVE-2020-9490</a>

<p>Felix Wilhelm a découvert qu'une valeur de l'en-tête 'Cache-Digest',
contrefaite pour l'occasion, dans une requête HTTP/2 pourrait provoquer un
plantage quand le serveur essaye ensuite de réaliser effectivement un
« HTTP/2 PUSH » d'une ressource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11984">CVE-2020-11984</a>

<p>Felix Wilhelm a signalé un défaut de dépassement de tampon dans le
module mod_proxy_uwsgi qui pourrait avoir pour conséquence la divulgation
d'informations ou éventuellement l'exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11993">CVE-2020-11993</a>

<p>Felix Wilhelm a signalé que, quand trace ou debug sont activés pour le
module HTTP/2, certains patrons de trafic périphérique peuvent provoquer
l'enregistrement d'instructions sur la mauvaise connexion, causant
l'utilisation simultanée de zones de mémoires.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.4.38-3+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4757.data"
# $Id: $
