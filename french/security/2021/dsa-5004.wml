#use wml::debian::translation-check translation="9021424ee668597a5b8130c5d1bb387ddc66dac3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans XStream,
une bibliothèque Java pour sérialiser des objets en XML et les
désérialiser.</p>

<p>Ces vulnérabilités peuvent permettre à un attaquant distant de charger
et exécuter du code arbitraire à partir d'un hôte distant simplement en
manipulant le flux d’entrée traité.</p>

<p>XStream définit lui-même désormais une liste d'autorisations par défaut,
c'est-à-dire que toutes les classes sont bloquées sauf les types pour
lesquels des convertisseurs sont explicitement déclarés. Habituellement, une
liste d'interdictions existait par défaut, c'est-à-dire qu'il essayait de
bloquer toutes les classes critiques actuellement connues de
l'environnement d'exécution de Java. La raison principale de la liste
d'interdictions était la compatibilité qui permettait d'utiliser les 
nouvelles versions d'XStream comme substitution. Toutefois, cette approche
a échoué. Une liste croissante de rapports de sécurité a démontré qu'une
liste d'interdictions est intrinsèquement non sûre, sans tenir compte du
fait que les types des bibliothèques tierces n'étaient même pas envisagés.
Un scénario de liste d'interdictions devrait être évité en général, car il
procure un sentiment de sécurité trompeur.</p>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 1.4.11.1-1+deb10u3.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1.4.15-3+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxstream-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxstream-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5004.data"
# $Id: $
