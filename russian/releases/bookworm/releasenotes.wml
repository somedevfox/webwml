#use wml::debian::template title="Debian 12 &mdash; Информация о выпуске" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#use wml::debian::translation-check translation="33765739bb1fc08542881115f973fb8bd4ab5777" maintainer="Lev Lamberov"

<if-stable-release release="buster">
<p>Информации о выпуске для Debian 12, кодовое имя bookworm, пока нет.</p>
</if-stable-release>

<if-stable-release release="bullseye">
<p>Это <strong>не окончательная версия</strong> информации о выпуске
Debian 12, кодовое имя bookworm, который пока не вышел. Представленная
здесь информация может быть неточной и устаревшей, и, по всей видимости,
она не полна.</p>
</if-stable-release>

<p>Чтобы узнать, что нового появилось в Debian 12, смотрите информацию о выпуске для вашей
архитектуры:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Информация о выпуске'); :>
</ul>

<p>Информация о выпуске также содержит инструкции для обновления с предыдущего выпуска
дистрибутива.</p>

<p>Если в вашем браузере верно установлены параметры вашего языка,
вы можете использовать приведенную выше ссылку для автоматического получения HTML-версии
информации о выпуске на вашем языке &mdash; смотрите <a href="$(HOME)/intro/cn">информацию о согласовании содержания</a>.
В противном случае выберите архитектуру, язык и формат из
таблицы ниже.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Архитектура</strong></th>
  <th align="left"><strong>Формат</strong></th>
  <th align="left"><strong>Языки</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
