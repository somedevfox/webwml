#use wml::debian::template title="Установка Debian через Интернет" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83" maintainer="Lev Lamberov"

<p>Этот метод установки Debian требует работающего Интернет соединения
<em>в процессе</em> установки. В результате, по сравнению с другими методами,
вы загрузите меньше данных, так как процесс будет соответствовать вашим требованиям.
Поддерживаются Ethernet и беспроводные подключения. К сожалению, внутренние ISDN карты
<em>не</em> поддерживаются.</p>
<p>Есть три варианта установки по сети:</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">Маленькие диски или USB-накопители</toc-add-entry>

<p>Ниже указаны ссылки на файлы образов.
Выберите ниже архитектуру вашего процессора.</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>Подробности смотрите на странице <a href="../CD/netinst/">Сетевая установка
с минимального CD</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">Мини CD, USB-накопители и т.д.</toc-add-entry>

<p>Вы можете загрузить несколько файлов образов небольшого размера, подходящих для
USB-диска и других подобных устройств, записать их на носитель
и начать установку, загрузившись с него.</p>

<p>Есть некоторые отличия в поддержке установки с разных
очень маленьких образов на разных архитектурах.
</p>

<p>Подробности смотрите на странице
<a href="$(HOME)/releases/stable/installmanual">руководства по установке
для вашей архитектуры</a>, в частности, главу
<q>Получение носителя с дистрибутивом</q>.</p>

<p>
Ниже указаны ссылки на доступные файлы
образов (дополнительную информацию смотрите в файле MANIFEST):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">Сетевая загрузка</toc-add-entry>

<p>Вам нужно настроить серверы TFTP и DHCP (или BOOTP, RARP), которые будут
предоставлять установочные носители машинам в вашей локальной сети. Вы можете запустить
программу установки Debian (с помощью TFTP и PXE) и продолжить установку
по сети, если BIOS вашей клиентской машины поддерживает такую функцию.</p>

<p>Не все машины поддерживают загрузку по сети. Так как в данном случае
необходима дополнительная работа, этот метод установки Debian не рекомендуется
новичкам.</p>

<p>Подробности смотрите на странице
<a href="$(HOME)/releases/stable/installmanual">руководства по установке
для вашей архитектуры</a>, в частности, главу
<q>Подготовка файлов для загрузки по TFTP</q>.</p>
<p>Ниже указаны ссылки на файлы образов (дополнительную информацию смотрите
в файле MANIFEST):</p>

<stable-netboot-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">
<p>
Если для какого-то оборудования в вашей системе <strong>требуется загрузка несвободных
микропрограмм</strong> вместе с драйвером устройства, то вы можете использовать один из
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tar-архивов распространённых пакетов с микропрограммами</a> или загрузить <strong>неофициальный</strong> образ,
включающий эти <strong>несвободные</strong> микропрограммы. Инструкции о том, как использовать эти tar-архивы,
а также общую информацию о загрузке микропрограмм во время установки можно найти
в <a href="../releases/stable/amd64/ch06s04">руководстве по установке</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">неофициальные
установочные образы для <q>стабильного</q> выпуска с микропрограммами</a>
</p>
</div>
