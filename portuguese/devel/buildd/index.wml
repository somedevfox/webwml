#use wml::debian::template title="Rede autobuilder" BARETITLE="true"
#use wml::debian::translation-check translation="cc0db4d4087a4f97b1a3955e3ca0f84b31caf8a9"

<p>A rede autobuilder (construtores automáticos) é um desenvolvimento Debian que
gerencia as compilações de pacotes para todas as arquiteturas que o
<a href="$(HOME)/ports/">Debian suporta atualmente</a>. Esta
rede é feita de várias máquinas que usam um pacote de software
específico chamado <em>buildd</em> para pegar os pacotes de um
repositório Debian e reconstruí-los para a arquitetura alvo.</p>

<h2>Por que a rede autobuilder é necessária?</h2>

<p>A distribuição Debian suporta <a href="$(HOME)/ports/">algumas
arquiteturas</a>, mas os(as) mantenedores(as) de pacote usualmente só
compilam as versões binárias para uma única arquitetura
(normalmente i386 ou amd64). As outras construções são
produzidas automaticamente, garantindo que cada pacote seja
construído apenas uma vez. As falhas são rastreadas no banco
de dados do autobuilder.</p>

<p>
Quando o Debian/m68k (o primeiro porte não Intel) começou, os(as)
desenvolvedores(as) tinham que ficar atentos a novas versões de pacotes
e recompilá-los se quisessem mantê-los atualizados com a distribuição
da Intel. Tudo isso era feito manualmente: desenvolvedores(as) olhavam a
lista de discussão upload em busca de novos pacotes e pegavam
alguns deles para construção. A coordenação para que nenhum pacote fosse
construído duas vezes por diferentes pessoas era feita através de
anúncio em uma lista de discussão. É óbvio que este procedimento é
suscetível a erros e consome muito tempo. No entanto, essa foi a maneira
usual de manter as distribuições não i386 atualizadas por muito tempo.
</p>

<p>
O sistema de daemon de construção automatiza a maioria deste processo.
Ele consiste em um conjunto de scripts (escritos em Perl e Python) que tem
evoluído através do tempo para ajudar portadores em várias tarefas.
Eles finalmente desenvolveram um sistema que é capaz de manter
distribuições Debian atualizadas quase que automaticamente. As atualizações de
segurança são criadas no mesmo conjunto de máquinas para garantir sua
disponibilidade oportuna.
</p>


<h2>Como o buildd funciona?</h2>

<p><em>Buildd</em> é o nome usualmente dado ao software usado pela
rede autobuilder, mas na verdade ele é feito de diferentes partes:</p>

<dl>

<dt>wanna-build</dt>
<dd>
uma ferramenta que ajuda a coordenar a (re)construção de pacotes através
de um banco de dados que mantém uma lista de pacotes e seus estados. Há
um banco de dados central por arquitetura que armazena os estados dos
pacotes, versões, e algumas outras informações. Ele é abastecido com
arquivos de fontes e pacotes recuperados dos vários arquivos de pacotes
que o Debian possui (por exemplo, ftp-master e security-master).
</dd>

<dt><a href="https://packages.debian.org/buildd">buildd</a></dt>
<dd>
um daemon que, periodicamente, verifica o banco de dados mantido pelo
<em>wanna-build</em> e chama o <em>sbuild</em> para construir os pacotes.
Após o log de construção ter sido reconhecido pelo(a) administrador(a), o
pacote é enviado para o repositório apropriado.
</dd>

<dt><a href="https://packages.debian.org/sbuild">sbuild</a></dt>
<dd>
é responsável pela compilação real dos pacotes em chroots isoladas.
Ele garante que todas as dependências de fontes necessárias sejam
instaladas no chroot antes da compilação e depois chama as ferramentas
padrão do Debian para iniciar o processo de compilação. Os logs de
construção são enviados ao
<a href="https://buildd.debian.org">banco de dados de logs de construção</a>.
</dd>

</dl>

<p>Todas estas partes <a href="operation">operam</a>
juntas para fazer a rede de construtores funcionar.</p>

<h2>O que um(a) desenvolvedor(a) Debian precisa fazer?</h2>

<p>Na verdade, um(a) desenvolvedor(a) Debian mediano(a) não precisa fazer nada
explicitamente para usar a rede buildd. Quando ele(a) envia um pacote para
o repositório (binário compilado para uma dada arquitetura) ele será
adicionado ao banco de dados para todas as arquiteturas (no estado
<em>Needs-Build</em>).
Máquinas construtoras consultarão o banco de dados de construção em
busca de pacotes neste estado e, rotineiramente, pegarão pacotes desta
lista. A lista é priorizada pelo estado de compilação anterior
(ou <em>out-of-date</em> ou <em>uncompiled</em>), prioridade,
seção e nome do pacote. Além disso, para impedir que alguns pacotes
passem tempo demais no final da fila, as prioridades são ajustadas
dinamicamente com o aumento do tempo de espera na fila.</p>

<p>Se a construção for bem sucedida em todas as arquiteturas,
o(a) mantenedor(a) não precisará fazer nada. Todos esses pacotes binários serão
enviados para o repositório correspondente. Se o processo de construção
falhar, o pacote entrará em um estado especial (<em>Build-Attempted</em>
para falhas de construção que não foram revisadas, <em>Failed</em> para
erros revisados e relatados no pacote ou <em>Dep-Wait</em>, se ele possui
dependências de construção específicas que não estão disponíveis).
Os(As) administradores(as) da rede autobuilder revisarão os
pacotes cuja construção falhou e entrarão em contato com o(a) mantenedor(a),
usualmente, abrindo um bug no sistema de acompanhamento de bug.</p>

<p>Algumas vezes um pacote leva um longo tempo para construir em uma dada
arquitetura e isso impede que o pacote entre na
<a href="$(HOME)/devel/testing">testing</a>. Se um pacote segurar uma
transição, as prioridades de construção são geralmente ajustadas mediante
solicitação da equipe de lançamento. Outras solicitações não serão
aceitas, pois o aumento do tempo de espera na fila resultará em uma
prioridade de construção mais alta automaticamente.</p>

<p>Você pode verificar esse estado das diferentes tentativas das buildds
dos pacotes que pertencem a qualquer mantenedor(a) verificando os
<a href="https://buildd.debian.org/status/">logs do buildd</a>.
Estes logs também estão linkados a partir da visão geral do(a) mantenedor(a)
do pacote.</p>

<p>Para mais informação sobre os diferentes estados nos quais um pacote pode
estar, por favor leia <a href="wanna-build-states">wanna-build-states</a>.</p>

<h2>Onde eu posso encontrar informações adicionais?</h2>

<p>É claro que, tanto a documentação quando o código fonte disponíveis
para estas diferentes ferramentas são a melhor forma de entender como a
rede buildd funciona. Adicionalmente, a seção
<a href="$(HOME)/doc/manuals/developers-reference/pkgs.html#porting">\
portando e sendo portado</a> da
<a href="$(HOME)/doc/manuals/developers-reference/">referência do(a)
desenvolvedor(a) Debian</a> fornece informação complementar sobre como
a rede funciona e também fornece alguma informação sobre
<a href="$(HOME)/doc/manuals/developers-reference/tools.html#tools-builders">\
construtores de pacotes</a> e
<a href="$(HOME)/doc/manuals/developers-reference/tools.html#tools-porting">\
ferramentas de porte</a> que estão envolvidas no processo tanto de
configuração quanto de manutenção da rede buildd.</p>

<p>Há algumas estatísticas disponíveis sobre a rede autobuilder na
<a href="https://buildd.debian.org/stats/">página de estados do buildd</a>.</p>

<h2>Como eu posso configurar meu próprio nó de auto-builder?</h2>

<p>Há várias razões pelas quais um(a) desenvolvedor(a) (ou usuário(a))
pode querer configurar e rodar um autobuilder:</p>

<ul>
<li>Para ajudar no desenvolvimento de um porte para uma dada arquitetura
(quanto autobuilders são necessários).</li>
<li>Para avaliar o impacto de uma dada otimização de compilador ou patch
pela recompilação de um grande subconjunto de pacotes.</li>
<li>Para rodar ferramentas que analisam pacotes em busca de erros
comuns e precisam rodar em pacotes compilados. Isto é necessário
até mesmo na realização de análise de código fonte, por exemplo,
como uma forma de contornar pacotes usando <tt>dpatch</tt>.</li>
</ul>

<p>Você pode ler mais informação sobre como
<a href="https://wiki.debian.org/BuilddSetup">configurar um autobuilder</a>.</p>

<h2>Contatando os(as) administradores(as) das buildds</h2>

<p>Os(As) administradores(as) responsáveis pelos buildds para uma arquitetura
específica podem ser contatados(as) em <email arch@buildd.debian.org>,
por exemplo <email i386@buildd.debian.org>.

<hrline />
<p><small>Esta introdução à rede autobuilder foi escrita com bits e pedaços
fornecidos por Roman Hodek, Christian T. Steigies, Wouter Verhelst, Andreas Barth,
Francesco Paolo Lovergine e Javier Fernández-Sanguino.</small></p>
