#use wml::debian::template title="Porte para PowerPC -- História" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/menu.inc"
#use wml::debian::translation-check translation="70cf45edbaeb4b8fc8f99d683f2f5c5c4435be92"

<br>
<br>

<h1>História do Debian/PowerPC</h1>

<p>
 O porte para PowerPC iniciou em 1997 após o <a
 href="http://www.linux-kongress.org/">Linux Kongress</a> alemão em
 W&uuml;rzburg, onde o projeto Debian recebeu um <a
 href="http://www.infodrom.north.de/Infodrom/tervola.html">PowerPC</a>
 patrocinado para questões de desenvolvimento.
</p>

<p>
 Felizmente, boot- e rootdisks funcionais foram encontrados em <a
 href="http://www.linuxppc.org/">LinuxPPC</a> e nós podíamos tentar
 instalar algo na máquina. Infelizmente, isto requeria alguns
 programas que somente rodavam sob o Mac OS. A única maneira de
 instalá-los foi pegar outra máquina rodando o Mac OS que já os tinha
 instalados. Como a troca de dados com outras máquinas não Mac OS somente
 era possível através de disquetes formatados em msdos, isto foi um
 problema do tipo "quem vem primeiro, o ovo ou a galinha".
</p>

<p>
 De alguma forma, conseguimos conectar um outro disco à máquina e
 instalar o Linux nela. Isto foi a famosa DR1 da Apple. De repente, nós
 começamos a portar o dpkg e agreadados para o novo sistema. Uau, aquilo
 era possível. Foi impressionante como os primeiros pacotes foram portados
 e construídos. Infelizmente a DR1 não vinha com um vinculador (linker) dinâmico
 e bibliotecas compartilhadas. Outro problema era que as bibliotecas e
 os arquivos de cabeçalho pareciam estar um pouco desatualizados e nós não éramos
 capazes de compilar um novo libc de imediato. Contudo, o maior e mais
 sério problema era essas quebras suspeitas que também estragavam
 o sistema de arquivos por inteiro. Corrigível somente por reinstalações.
</p>

<p>
 Klee Dienes, outro desenvolvedor Debian, trabalhou neste problema e
 de repente veio com um arquivo <a
 href="ftp://ftp.infodrom.north.de/pub/Linux/linux-pmac/debian/mklinuxfs.tar.gz">tarball mklinux</a>
 de um sistema mais recente - um Debian GNU/Linux modificado.  Este
 tarball veio com uma versão 1.99 antiga do libc.  Deste momento em diante
 a máquina rodou de maneira estável na rede e nós conseguimos continuar nossos
 esforços. Nós estávamos compilando vários pacotes e percebemos que alguns
 arquivos de cabeçalho não eram apropriados e que muitos programas não
 foram compilados pelo compilador normal.
</p>

<p>
 Então, o Joel Klecker, um novo desenvolvedor Debian à época, começou a
 trabalhar no egcs e sua compilação na máquina PowerPC. Após isto ter
 sido feito, o próximo passo foi trabalhar nas versões atuais do libc.
 Deu-se que nossa versão libc-1.99 era incompatível com a próxima versão
 pre2.1 que era necessária de modo a mover o porte para o estado
 estável.
</p>

<p>
 Neste estágio, Hartmut Koptein trocou do m68k para o porte para
 powerpc e iniciou o desenvolvimento dele. Versões de desenvolvimento
 do Debian/PowerPC estavam disponíveis em 1998 e 1999.
</p>

<p>
 O porte foi oficialmente lançado pela primeira vez com o Debian GNU/Linux
 2.2 ("potato") em agosto de 2000.
</p>

<p>
 O desenvolvimento do porte continua. Um daemon de construção foi configurado
 em voltaire.debian.org,
 uma máquina PowerPC doada por Daniel Jacobowitz, também um desenvolvedor
 Debian.
</p>
