#use wml::debian::template title="Debian som desktop"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fa8e80162815f92b0d792ca4837df20cdc61c896"

<h2>Det universelle styresystem som din desktop</h2>

<p>
  Debian Desktop-underprojektet er en gruppe af frivillige som ønsker at 
  fremstille det bedst mulige styrestystem til brug som arbejdsmaskine i 
  hjem og virksomheder.  Vores motto er <q>Software som bare virker</q>.  Kort 
  fortalt er vores mål at gøre Debian, GNU og Linux anvendelig blandt 
  almindelige brugere.
  <img style="float: right;" src="debian-desktop.png" alt="Debian Desktop" />
</p>

<h3>Vore principper</h3>

<ul>
  <li>
    Da der findes mange <a href="https://wiki.debian.org/DesktopEnvironment">\
    skrivebordsmiljøer</a>, understøtter vi brugen af dem, og sikrer at de 
    fungerer godt i Debian.</li>
  <li>
    Vi er opmærksomme på at der kun er to vigtige typer brugere: begynderen og 
    eksperten.  Vi vil gøre så meget som muligt, for at gøre det nemt at være
    nybegynder, mens eksperter der har lyst til det får lov til at justere på 
    alt.
  </li>
  <li>
    Vi vil prøve at sikre, at programmerne er opsat til det mest udbredte
    desktop-anvendelse.  For eksempel skal de almindelige brugerkonti som blive 
    tilføjet som standard under installationen, havde rettigheder til at 
    afspille lyd og film, udskrive, og vedligeholde systemet ved hjælp af sudo.
  </li>
  <li>
    <p>
      Vi vil prøve at sikre, at spørgsmål som brugeren bliver præsenteret for
      (hvilket bør være minimalt) giver mening, selv med et minimum af 
      viden om computere.  Nogle Debian-pakker stiller i dag brugeren spørgsmål
      med svært forståelige tekniske oplysninger.  Tekniske debconf-spørgsmål, 
      som præsenteres for brugeren af debian-installer, bør undgås.  For 
      nybegynderen er de ofte forvirrende og skræmmende.  For eksperten kan de 
      være irriterende og unødvendige.  En nybegynder ved måske end ikke hvad 
      spørgsmålene handler om.  En ekspert kan opsætte sit skrivebordsmiljø på 
      en hvilken som helst måde, vedkommende måtte ønske, efter installeringen 
      er gennemført.  Som minimum bør prioriteringen af disse former for 
      debconf-spørgsmål nedsættes.
    </p>
  </li>
  <li>
    Og vi vil synes at det er sjovt at gøre det hele!
  </li>
</ul>

<h3>Hvordan du kan hjælpe til</h3>

<p>
  De vigtigste dele af et Debian-underprojekt er ikke postlister, websider, 
  eller arkivplads til pakker.  Det vigtigste er <em>motiverede deltagere</em>
  som kan få sat skub i tingene.  Du behøver ikke at være officiel udvikler, 
  for at begynde at fremstille pakker og rettelser.  Debian Desktops 
  kernegruppe vil sørge for at dit arbejde blive integreret.  Her er derfor
  nogle ting du kan gøre:
</p>

<ul>
  <li>
    Test vores <q>Desktop Default Environment</q>-task (eller 
    kde-desktop-task), ved at installere af af vores 
    <a href="$(DEVEL)/debian-installer/">testaftryk af den næste udgave</a> og
    send reaktioner til 
    <a href="https://lists.debian.org/debian-desktop/">postlisten 
    debian-desktop</a>.
  </li>
  <li>
    Arbejd på <a href="$(DEVEL)/debian-installer/">debian-installer</a>.  
    GTK+-frontend'en har brug for dig.
  </li>
  <li>
    Hjælp 
    <a href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME Team</a>
    <a href="https://qt-kde-team.pages.debian.net/">Debian Qt og KDE Team</a> eller
    <a href="https://salsa.debian.org/xfce-team/">Debian Xfce Group</a>.  Du kan 
    hjælpe med pakning, fejlsortering, dokumentation, test og meget mere.
  </li>
  <li>
    Undervis brugere i hvordan man installerer og anvender de 
    Debian-desktoptasks vi har nu (desktop, gnome-desktop og kde-desktop).
  </li>
  <li>
    Arbejd på at prioriteringsnedsættelse eller fjernelse af unødvendige 
    <a href="https://packages.debian.org/debconf">debconf</a>-spørgsmål fra
    pakker, og gør dem der er nødvendige, nemme at forstå.
  </li>
  <li>
    Hjælp med i <a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian
    Desktop Artwork</a>-arbejdet.
  </li>
</ul>

<h3>Wiki</h3>

<p>Vi har nogle artikler på vores wiki, og vores udgangspunkt der er:
<a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>. Nogle 
Debian Desktop-wikiartikler er forældede.</p>

<h3>Postliste</h3>

<p>Projektet diskuteres på postlisten
<a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.</p>

<h3>IRC-kanal</h3>

<p>Vi opfordrer alle (Debian-udviklere eller ej) som er interesserede i Debian
Desktop, til at slutte sig til kanalen #debian-desktop på
<a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org).</p>

<h3>Hvem er involveret?</h3>

<p>Alle der har lyst til at deltage er velkomne.  Faktisk er alle i grupperne
pkg-gnome, pkg-kde og pkg-xfce indirekte involveret.  Dem der har tegnet 
abonnement på postlisten debian-desktop er aktive bidragydere.  Grupperne
Debian-installer og tasksel er også vigtige for vores mål.</p>

<p>Denne webside vedligeholdes af 
<a href="https://people.debian.org/~stratus/">Gustavo Franco</a>.  
Forhenværende vedligeholdere er 
<a href="https://people.debian.org/~madkiss/">Martin Loschwitz</a> og
<a href="https://people.debian.org/~walters/">Colin Walters</a>.</p>
