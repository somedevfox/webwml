#use wml::debian::template title="Toegankelijk Debian"
#use wml::debian::translation-check translation="82f3d1721149ec88f5974028ed2c2d8d454b4071"
{#style#:<link rel="stylesheet" href="style.css" type="text/css" />:#style#}

<h2>Projectbeschrijving</h2>

<p>Toegankelijk Debian is een intern project om Debian te ontwikkelen tot
   een besturingssysteem dat bijzonder geschikt is voor de behoeften van
   mensen met een handicap.
   Het doel van Toegankelijk Debian is een volledig op vrije software
   gebaseerd en volkomen toegankelijk systeem te bouwen dat gebruikers met
   een handicap de hoogst mogelijke mate van onafhankelijkheid biedt.
</p>
<p>We denken dat Toegankelijk Debian bestaande pakketten extra waardevol zal
   maken door patches aan te bieden voor het oplossen van problemen die zeer
   specifiek zijn voor bepaalde gebruikersgroepen of door
   toegankelijkheidsvalideringstesten uit te voeren en op basis van de
   resultaten ervan suggesties voor aanpassingen te doen.
</p>

<h2><a id="email-list" name="email-list">E-maillijst</a></h2>

<p>De mailinglijst Debian-Accessibility is het centrale communicatiepunt voor
   Toegankelijk Debian. Hij fungeert als een forum voor potentiële en huidige
   gebruikers van het Debian-systeem die speciale behoeften hebben. Daarnaast
   wordt hij gebruikt om ontwikkelingsinspanningen rond de verschillende
   onderwerpen van toegankelijkheid te coördineren. U kunt zich aan- en
   afmelden voor de mailinglijst via
   <a href="https://lists.debian.org/debian-accessibility/">de webpagina van de
   lijst</a>, en u kunt ook de
   <a href="https://lists.debian.org/debian-accessibility/">lijstarchieven</a>
   lezen.
</p>

<h2><a id="projects" name="projects">Relevante software</a></h2>

<p>De eerste poging om de software in categorieën onder te brengen is misschien
   niet de meest geslaagde. Stuur suggesties voor verbeteringen naar
   <a href="#email-list">de mailinglijst</a>, of naar
   <a href="mailto:sthibault@debian.org">Samuel Thibault</a>.
</p>
<ul>
  <li><a href="software#speech-synthesis">Spraaksynthese en daarmee verband
      houdende APIs</a></li>
  <li><a href="software#console">Schermlezers voor de console (tekstmodus)</a></li>
  <li><a href="software#emacs">Scherminspectie-extensies voor Emacs</a></li>
  <li><a href="software#gui">Grafische gebruikersomgevingen</a>:
      <ul>
        <li><a href="software#gnome">Toegankelijk gemaakt GNOME</a></li>
        <li><a href="software#input">Niet-standaard invoermethoden</a></li>
      </ul></li>
</ul>

<h2><a id="goals" name="goals">Projectdoelstellingen</a></h2>

<ul>
  <li>Verstrekken van informatie en documentatie over toegankelijkheid.</li>
  <li>Ervoor zorgen dat toegankelijkheidssoftware, zoals stuurprogramma's voor
      gespecialiseerde randapparatuur, indien nodig zo vroeg mogelijk kan
      geladen worden in de opstartfase van het systeem, met inbegrip van het
      installatieproces van Debian. Dit is bedoeld om ervoor te zorgen
      dat mensen met speciale noden een hoge mate van onafhankelijkheid
      behouden bij het onderhouden van hun eigen systemen.</li>
  <li>Erop toezien en ervoor zorgen dat de kerninfrastructuur van Debian, zoals
      onze website, voldoet aan de toegankelijkheidsrichtlijnen.</li>
  <li>Auteurs van verschillende projecten met gelijkaardige doelstellingen
      samenbrengen.</li>
  <li>Bovenstroomse auteurs helpen om hun producten verpakt te krijgen voor
      Debian.</li>
  <li>Commerciële leveranciers van ondersteunende technologie de sterke punten
      van op Vrije Software gebaseerde systemen laten zien en hen erover laten
      nadenken om hun software geschikt te maken voor Linux of zelfs over te
      stappen op openbronsoftware.</li>
</ul>

<h2><a id="help" name="help">Wat kan ik doen om te helpen?</a></h2>

<ul>
  <li>Werken aan de verbetering en de vertaling van deze webpagina's.</li>
  <li>Een logo ontwerpen.</li>
  <li>Documentatie schrijven en vertalingen maken.</li>
  <li>Internationalizering (wat meer inhoudt dan louter vertalen, zie
      <a href="software#i18nspeech">geïnternationaliseerde spraaksynthese</a>
      voor enkele ideeën).</li>
  <li>Controleren of de website debian.org toegankelijk is volgens de
      vastgestelde richtlijnen voor toegankelijkheid en op basis van uw
      bevindingen suggesties voor verbetering doen. Het kan uiteindelijk
      wenselijk zijn om ooit zoiets als het label Bobby Approved aan te
      vragen.</li>
</ul>

<h2><a id="links" name="links">Links</a></h2>

<ul>
  <li>De <a href="https://wiki.debian.org/accessibility">Wikipagina van Toegankelijk Debian</a>.</li>
  <li>De <a href="https://wiki.debian.org/accessibility-maint">Wikipagina voor alle pakketbeheerders van Debian over Toegankelijk Debian</a>.</li>
  <li>De <a href="https://wiki.debian.org/accessibility-devel">Wikipagina voor ontwikkelaars van Toegankelijk Debian</a>.</li>
  <li>De <a href="https://wiki.linuxfoundation.org/accessibility/start">toegankelijkheidswerkgroep van de vrije-standaardengroep</a>.</li>
  <li>Het <a href="https://wiki.gnome.org/Accessibility">toegankelijkheidsproject van GNOME</a>.</li>
  <li>Het <a href="https://community.kde.org/Accessibility">toegankelijkheidsproject van KDE</a>.</li>
  <li><a href="https://wiki.ubuntu.com/Accessibility">Toegankelijk Ubuntu</a></li>
  <li><a href="http://leb.net/blinux/">BLINUX</a>: de bruikbaarheid van het GNU/Linux-besturingssysteem voor blinde gebruikers verbeteren.</li>
  <li><a href="http://www.linux-speakup.org/">Het Linux Speakup-project</a>.</li>
  <li><a href="http://www.w3.org/WAI/">Initiatief van het W3C inzake webtoegankelijkheid (Web Accessibility Initiative - WAI)</a>:
      <ul>
        <li>In de <a href="http://www.w3.org/TR/WCAG10/">richtlijnen voor
        toegankelijkheid van webinhoud</a> wordt in detail uitgelegd hoe een
        website toegankelijk kan worden gemaakt voor mensen met uiteenlopende
        handicaps.</li>
        <li>De <a href="http://www.w3.org/TR/ATAG10/">richtlijnen voor
        toegankelijkheid van hulpmiddelen voor auteurs</a> leggen uit hoe een
        verschillende auteurshulpmiddelen de productie van toegankelijke
        webinhoud kunnen ondersteunen, en ook hoe de software zelf toegankelijk
        kan worden gemaakt.</li>
      </ul></li>
</ul>
