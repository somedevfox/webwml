<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>This update ships updated CPU microcode for some types of Intel CPUs and
provides mitigations for security vulnerabilities which could result in
privilege escalation in combination with VT-d and various side channel
attacks.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 3.20210608.2~deb10u1.</p>

<p>Note that there are two reported regressions; for some CoffeeLake CPUs
this update may break iwlwifi
(https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/issues/56)
and some for Skylake R0/D0 CPUs on systems using a very outdated firmware/BIOS,
the system may hang on boot:
(https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/issues/31)</p>

<p>If you are affected by those issues, you can recover by disabling microcode
loading on boot (as documented in README.Debian, also available online at
<a href="https://salsa.debian.org/hmh/intel-microcode/-/blob/master/debian/README.Debian">\
https://salsa.debian.org/hmh/intel-microcode/-/blob/master/debian/README.Debian)</a></p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>For the detailed security status of intel-microcode please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4934.data"
# $Id: $
