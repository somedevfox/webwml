<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Sze Yiu Chau and his team from Purdue University and The University of
Iowa found several security issues in the gmp plugin for strongSwan,
an IKE/IPsec suite.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16151">CVE-2018-16151</a>

    <p>The OID parser in the ASN.1 code in gmp allows any number of random
    bytes after a valid OID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16152">CVE-2018-16152</a>

    <p>The algorithmIdentifier parser in the ASN.1 code in gmp doesn't
    enforce a NULL value for the optional parameter which is not used
    with any PKCS#1 algorithm.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.2.1-6+deb8u7.</p>

<p>We recommend that you upgrade your strongswan packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1522.data"
# $Id: $
