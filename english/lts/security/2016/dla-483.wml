<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Gustavo Grieco discovered that Expat, a XML parsing C library, does not
properly handle certain kinds of malformed input documents, resulting in
buffer overflows during processing and error reporting. A remote
attacker can take advantage of this flaw to cause an application using
the Expat library to crash, or potentially, to execute arbitrary code
with the privileges of the user running the application.</p>


<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.1.0-1+deb7u3.</p>

<p>We recommend that you upgrade your expat packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-483.data"
# $Id: $
