<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a directory traversal attack in pip, the
Python package installer.</p>

<p>When an URL was given in an install command, as a
<tt>Content-Disposition</tt> header was permitted to have <tt>../</tt>
components in their filename, arbitrary local files (eg.
<tt>/root/.ssh/authorized_keys</tt>) could be overidden.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20916">CVE-2019-20916</a>

    <p>The pip package before 19.2 for Python allows Directory Traversal when a
    URL is given in an install command, because a Content-Disposition header
    can have ../ in a filename, as demonstrated by overwriting the
    /root/.ssh/authorized_keys file. This occurs in _download_http_url in
    _internal/download.py.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
9.0.1-2+deb9u2.</p>

<p>We recommend that you upgrade your python-pip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2370.data"
# $Id: $
