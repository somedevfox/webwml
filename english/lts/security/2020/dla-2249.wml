<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following CVE(s) were reported against src:libexif.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0182">CVE-2020-0182</a>

    <p>In exif_entry_get_value of exif-entry.c, there is a possible
    out of bounds read due to a missing bounds check. This could
    lead to local information disclosure with no additional execution
    privileges needed. User interaction is not needed for
    exploitation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0198">CVE-2020-0198</a>

    <p>In exif_data_load_data_content of exif-data.c, there is a
    possible UBSAN abort due to an integer overflow. This could lead
    to remote denial of service with no additional execution
    privileges needed. User interaction is needed for exploitation.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.6.21-2+deb8u4.</p>

<p>We recommend that you upgrade your libexif packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2249.data"
# $Id: $
