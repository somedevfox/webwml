<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several newly-referenced issues have been fixed in the FreeType 2 font
engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9381">CVE-2015-9381</a>

  <p>heap-based buffer over-read in T1_Get_Private_Dict in
  type1/t1parse.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9382">CVE-2015-9382</a>

  <p>buffer over-read in skip_comment in psaux/psobjs.c because
  ps_parser_skip_PS_token is mishandled in an FT_New_Memory_Face
  operation</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9383">CVE-2015-9383</a>

  <p>a heap-based buffer over-read in tt_cmap14_validate in
  sfnt/ttcmap.c</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.5.2-3+deb8u4.</p>

<p>We recommend that you upgrade your freetype packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1909.data"
# $Id: $
