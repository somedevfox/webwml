<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The original fix for <a href="https://security-tracker.debian.org/tracker/CVE-2019-12900">CVE-2019-12900</a> in bzip2, a high-quality
block-sorting file compressor, introduces regressions when extracting
certain lbzip2 files which were created with a buggy libzip2.
Please see <a href="https://bugs.debian.org/931278">https://bugs.debian.org/931278</a> for more information.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.0.6-4+deb7u2.</p>

<p>We recommend that you upgrade your bzip2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1833-2.data"
# $Id: $
