<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of clamav released as DLA 1953-1 led to permission issues on
/var/run/clamav. This caused several users to experience issues restarting the
clamav daemon. This regression is caused by a mistakenly backported patch from
the stretch package, upon which this update was based.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.101.4+dfsg-0+deb8u2.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1953-2.data"
# $Id: $
