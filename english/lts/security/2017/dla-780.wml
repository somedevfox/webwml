<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in libav:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7424">CVE-2016-7424</a>

    <p>The put_no_rnd_pixels8_xy2_mmx function in x86/rnd_template.c in
    libav 11.7 and earlier allows remote attackers to cause a denial
    of service (NULL pointer dereference and crash) via a crafted MP3
    file.</p></li>

<li>(No CVE assigned)

    <p>The h264 codec is vulnerable to various crashes with invalid-free,
    corrupted double-linked list or out-of-bounds read.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
6:0.8.19-0+deb7u1.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-780.data"
# $Id: $
