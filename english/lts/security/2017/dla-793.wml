<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A remote code execution vulnerability was discovered in opus, an audio
codec, that could enable an attacker using a specially crafted file to
cause memory corruption during media file and data processing.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.9.14+20120615-1+nmu1+deb7u1.</p>

<p>We recommend that you upgrade your opus packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-793.data"
# $Id: $
