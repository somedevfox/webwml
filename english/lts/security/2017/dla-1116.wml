<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that poppler, a PDF rendering library, was affected
by several denial-of-service (application crash), null pointer
dereferences and memory corruption bugs:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14517">CVE-2017-14517</a>

    <p>NULL Pointer Dereference in the XRef::parseEntry() function in
    XRef.cc</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14519">CVE-2017-14519</a>

    <p>Memory corruption occurs in a call to Object::streamGetChar that
    may lead to a denial of service or other unspecified impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14617">CVE-2017-14617</a>

    <p>Potential buffer overflow in the ImageStream class in Stream.cc,
    which may lead to a denial of service or other unspecified impact.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.18.4-6+deb7u3.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1116.data"
# $Id: $
