<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Numerous vulnerabilities were discovered in ImageMagick, an image
manipulation program. Issues include memory leaks, out of bound reads
and missing checks.</p>

<p>This update also includes an update of the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2016-8677">CVE-2016-8677</a> which
was incomplete in the previous version.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
8:6.7.7.10-5+deb7u11.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-807.data"
# $Id: $
