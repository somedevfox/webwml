<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Agostino Sarubbo of Gentoo discovered a heap buffer overflow write
in the rzip program when uncompressing maliciously crafted files.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.1-1+deb7u1.</p>

<p>We recommend that you upgrade your rzip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-955.data"
# $Id: $
