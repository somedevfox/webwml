<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>HTCondor, a distributed workload management system, has Incorrect Access
Control. It is possible to use a different authentication method to submit a
job than the administrator has specified. If the administrator has configured
the READ or WRITE methods to include CLAIMTOBE, then it is possible to
impersonate another user to the condor_schedd, for example to submit or remove
jobs.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
8.4.11~dfsg.1-1+deb9u1.</p>

<p>We recommend that you upgrade your condor packages.</p>

<p>For the detailed security status of condor please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/condor">https://security-tracker.debian.org/tracker/condor</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2724.data"
# $Id: $
