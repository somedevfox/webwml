<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Apache Log4j2, a Java Logging Framework, is vulnerable to a remote code
execution (RCE) attack where an attacker with permission to modify the logging
configuration file can construct a malicious configuration using a JDBC
Appender with a data source referencing a JNDI URI which can execute remote
code. This issue is fixed by limiting JNDI data source names to the java
protocol.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.12.4-0+deb9u1.</p>

<p>We recommend that you upgrade your apache-log4j2 packages.</p>

<p>For the detailed security status of apache-log4j2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apache-log4j2">https://security-tracker.debian.org/tracker/apache-log4j2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2870.data"
# $Id: $
