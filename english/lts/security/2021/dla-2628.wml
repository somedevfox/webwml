<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues have been discovered in python2.7:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16935">CVE-2019-16935</a>

    <p>The documentation XML-RPC server in Python 2.7 has XSS via the server_title
    field. This occurs in Lib/DocXMLRPCServer.py in Python 2.x, and in
    Lib/xmlrpc/server.py in Python 3.x. If set_server_title is called with
    untrusted input, arbitrary JavaScript can be delivered to clients that
    visit the http URL for this server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23336">CVE-2021-23336</a>

    <p>The Python2.7 vulnerable to Web Cache Poisoning via urllib.parse.parse_qsl
    and urllib.parse.parse_qs by using a vector called parameter cloaking. When
    the attacker can separate query parameters using a semicolon (;), they can
    cause a difference in the interpretation of the request between the proxy
    (running with default configuration) and the server. This can result in malicious
    requests being cached as completely safe ones, as the proxy would usually not
    see the semicolon as a separator, and therefore would not include it in a cache
    key of an unkeyed parameter.</p>

    <p>**Attention, API-change!**
    Please be sure your software is working properly if it uses `urllib.parse.parse_qs`
    or `urllib.parse.parse_qsl`, `cgi.parse` or `cgi.parse_multipart`.</p>

    <p>Earlier Python versions allowed using both  ``;`` and ``&`` as query parameter
    separators in `urllib.parse.parse_qs` and `urllib.parse.parse_qsl`.
    Due to security concerns, and to conform with
    newer W3C recommendations, this has been changed to allow only a single
    separator key, with ``&`` as the default.  This change also affects
    `cgi.parse` and `cgi.parse_multipart` as they use the affected
    functions internally. For more details, please see their respective
    documentation.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.7.13-2+deb9u5.</p>

<p>We recommend that you upgrade your python2.7 packages.</p>

<p>For the detailed security status of python2.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python2.7">https://security-tracker.debian.org/tracker/python2.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2628.data"
# $Id: $
