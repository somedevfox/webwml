<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in rabbitmq-server, a
message-broker software.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-4965">CVE-2017-4965</a>

    <p>Several forms in the RabbitMQ management UI are vulnerable to XSS
    attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-4966">CVE-2017-4966</a>

    <p>RabbitMQ management UI stores signed-in user credentials in a
    browser's local storage without expiration, making it possible to
    retrieve them using a chained attack</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-4967">CVE-2017-4967</a>

    <p>Several forms in the RabbitMQ management UI are vulnerable to XSS
    attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11281">CVE-2019-11281</a>

    <p>The virtual host limits page, and the federation management UI,
    which do not properly sanitize user input. A remote authenticated
    malicious user with administrative access could craft a cross site
    scripting attack that would gain access to virtual hosts and
    policy management information</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11287">CVE-2019-11287</a>

    <p>The "X-Reason" HTTP Header can be leveraged to insert a malicious
    Erlang format string that will expand and consume the heap,
    resulting in the server crashing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22116">CVE-2021-22116</a>

    <p>A malicious user can exploit the vulnerability by sending
    malicious AMQP messages to the target RabbitMQ instance.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.6.6-1+deb9u1.</p>

<p>We recommend that you upgrade your rabbitmq-server packages.</p>

<p>For the detailed security status of rabbitmq-server please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rabbitmq-server">https://security-tracker.debian.org/tracker/rabbitmq-server</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2710.data"
# $Id: $
