<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a remote authentication credential leak in
the "lynx" text-based web browser.</p>

<p>The package now correctly handles authentication subcomponents in URIs (eg.
<tt>https://user:pass@example.com</tt>) to avoid remote attackers discovering
cleartext credentials in SSL connection data.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38165">CVE-2021-38165</a>

    <p>	Lynx through 2.8.9 mishandles the userinfo subcomponent of a URI, which
    allows remote attackers to discover cleartext credentials because they may
    appear in SNI data.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, this problem has been fixed in version
2.8.9dev11-1+deb9u1.</p>

<p>We recommend that you upgrade your lynx packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2736.data"
# $Id: $
