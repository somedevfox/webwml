<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in libvorbis, a popular library for  
the Vorbis audio codec.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14160">CVE-2017-14160</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10393">CVE-2018-10393</a>

    <p>Improve bound checking for very low sample rates.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10392">CVE-2018-10392</a>

    <p>Validate the number of channels in vorbisenc.c</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.3.5-4+deb9u3.</p>

<p>We recommend that you upgrade your libvorbis packages.</p>

<p>For the detailed security status of libvorbis please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libvorbis">https://security-tracker.debian.org/tracker/libvorbis</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2828.data"
# $Id: $
